Rails.application.routes.draw do
  resources :events do
  	get 'subscribe'
  end
  get 'evtapp/:appuid', to: 'events#mobileshow'
  get 'events/preview/:id' => 'events#preview'
end
