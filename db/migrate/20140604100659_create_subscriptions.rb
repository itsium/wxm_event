class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.references :event, index: true
      t.references :user, index: true
      t.string :phone
      t.string :name
      t.string :email
      t.string :detail

      t.timestamps
    end
  end
end
