class CreateViewers < ActiveRecord::Migration
  def change
    create_table :viewers do |t|
      t.references :event, index: true
      t.references :user, index: true

      t.timestamps
    end
  end
end
