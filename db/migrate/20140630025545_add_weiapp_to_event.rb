class AddWeiappToEvent < ActiveRecord::Migration
  def up
    # add_column :events, :weiapp, :references
    # remove_column :events, :user
	change_table :events do |t|
		t.remove_references :user
		t.references :weiapp
	end
  end

  def down
    # remove_column :events, :weiapp
    # add_column :events, :user, :references
    change_table :events do |t|
    	t.remove_references :weiapp
		t.references :user
	end
  end
end
