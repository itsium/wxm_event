class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.string :region
      t.string :city
      t.string :district
      t.string :address
      t.datetime :start_at
      t.datetime :end_at
      t.integer :max_subscrib
      t.text :description
      t.float :price
      t.references :user, index: true

      t.timestamps
    end
  end
end
