class ChangeUserFromViewers < ActiveRecord::Migration
	def change
		change_table :viewers do |t|
			t.remove_references :user
			t.string :viewer_id, index: true
		end
	end
end
