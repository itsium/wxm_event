$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "wxm_event/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "wxm_event"
  s.version     = WxmEvent::VERSION
  s.authors     = ["itsium.cn"]
  s.email       = ["dev@itsium.cn"]
  s.homepage    = "http://itsium.cn"
  s.summary     = "Event organiser extension for wxm."
  s.description = "A social network based event organisation feature."

  s.files = Dir["{app,config,db,lib}/**/*", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.0.5"
  s.add_dependency "paperclip"
  s.add_dependency "jpbuilder"
  s.add_dependency 'kaminari-bootstrap', '~> 3.0.1'

  s.add_development_dependency "sqlite3"
end
