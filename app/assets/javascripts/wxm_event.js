(function() {

	var previous_subscription_id,
		win = window;

	var show = function(el) {
		//console.trace('hide: ', el);
		//$(el).addClass('show');
		el.style.display = 'table-row';
	};

	var hide = function(el) {
		//console.trace('hide: ', el);
		//$(el).removeClass('show');
		el.style.display = 'none';
	};

	var toggle_subscription = function(id) {

		if (previous_subscription_id) {

			var p = document.getElementById(previous_subscription_id);

			//p.style.display = 'none';
			hide(p);

			if (previous_subscription_id === id) {
				previous_subscription_id = false;
				return false;
			}
		}

		previous_subscription_id = id;

		var el = document.getElementById(id);

		//el.style.display = 'block';
		show(el);

	};

	var should_toggle = function(el) {

		//if (typeof el.target === 'undefined') return false;

		var sid = el.getAttribute('data-toggle-subscription');

		if (sid) {
			toggle_subscription(sid);
		}

	};

	win.addEventListener('click', function(evt) {
		should_toggle(evt.target);
	}, false);

}());
