class EventsController < InheritedResources::Base
	before_filter :wx_auth, except: [:wxlogin, :mobileshow]
	before_filter :check_user, only: [ :create, :update, :edit ]
    before_filter :create_viewer_cookie, only: [ :mobileshow ]
	layout "mobile", only: [:mobileshow]

    def create_viewer_cookie
        if cookies[:event_viewer].nil?
            cookies[:event_viewer] = {
                value: DateTime.now.strftime("%Y%j-") + Time.now.usec.to_s,
                expires: 1.year.from_now,
                httponly: true
            }
        end
    end

	def mobileshow
        @event = Weiapp.where(appuid: params[:appuid]).first().app
        has_view = @event.viewers.where(viewer_id: cookies[:event_viewer]).length > 0
        if has_view == false
            view = @event.viewers.new({
                viewer_id: cookies[:event_viewer]
            })
            view.save
        end
    end


    def show
        @event = Event.find(params[:id])
    end

    def create
        @event = Event.new(permitted_params[:event])
        @event.user = current_user
        create!
    end

    def update
        @event = Event.find(params[:id])
        @event.user = current_user

        @qr_link = "#{root_url}#{@event.weiapp.appuid}"
        @qr_for_link = gen_qrcode(@qr_link)

        update!
    end

    def destroy
        destroy! { a_url }
    end

    def edit
        @event = Event.find(params[:id])
        @subscriptions = @event.subscriptions.order('created_at DESC')
            .page(params[:page]).per(1)
        @qr_link = "#{root_url}#{@event.weiapp.appuid}"
        @qr_for_link = gen_qrcode(@qr_link)
    end

    def subscribe
        @event = Event.find(params[:event_id])
        nb_subscriptions = @event.subscriptions.length

        if @event.max_subscrib > nb_subscriptions
            @subscription = @event.subscriptions.new(permitted_params[:subscription])
            @subscription.user = current_user
            @subscription.save
        end
    end

	protected
	    def permitted_params
	        # TODO SET CORRECT FILTER
	        params.permit!
	    end
end
