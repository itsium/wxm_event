class Event < ActiveRecord::Base
  has_one :weiapp, as: :app, dependent: :destroy
  has_one :user, :through => :weiapp
  has_many :subscriptions, dependent: :destroy
  has_many :viewers, dependent: :destroy

  has_attached_file :image, :styles => { :original => "600x600>", :thumb => "130x130>" }#, :default_url => "/images/:style/question_missing.png"
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
  validates :max_subscrib, numericality: { only_integer: true, greater_than_or_equal_to: 0 }

  def display_name
    self.name
  end

  def display_description
    display_name
  end

  def identifier
    :event
  end

  def has_subscribed(current_user=nil)

    if current_user != nil
      return (self.subscriptions.where(user_id: current_user.id).length > 0)
    else
      return false
    end

  end

  def as_json(options=nil)

    item = super({ only: [
        :id, :name, :region, :city, :district,
        :address, :start_at, :end_at,
        :max_subscrib, :price, :description
    ] }.merge(options || {}))

    nb_subscribers = self.subscriptions.length

    if self.max_subscrib <= nb_subscribers
      is_full = true
    else
      is_full = false
    end

    item.merge({
      #has_subscribed: self.has_subscribed,
      is_full: is_full,
      posted_by: self.user.nickname,
      nb_subscribers: nb_subscribers,
      nb_viewers: self.viewers.length
    })

  end
end
